@echo off

if "%~1" == "" (
    @echo.
    @echo Sends PDF files from a directory and subdirectories to default printer
    @echo.
    @echo Usage: %0 dir_path
    @echo.
    @echo dir_path: Path of directory with PDF files to print
    pause
    goto :eof
)

pushd %1

if %errorlevel% NEQ 0 (
    goto :eof
)

for /f "delims=" %%f in ('dir /a-d-h-s /on /s /b *.pdf') do (
    PDFXCview.exe /print "%%f"
    echo Printed: %%f
)

popd

pause
