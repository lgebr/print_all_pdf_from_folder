## Print to PDF

Windows project to print all PDF files inside a folder automatically.

Initially was used [PDFtoPrinter](http://www.columbia.edu/~em36/pdftoprinter.html) but some files were not printing correctly so was changed to [SumatraPDF](https://www.sumatrapdfreader.org/) (Portable version).

Unfortunately [SumatraPDF](https://www.sumatrapdfreader.org/) is very slow to print. There's also a post about it in official SumatraPDF forum: [Too slow when printing](https://forum.sumatrapdfreader.org/t/too-slow-when-printing/2207).

Currently is in use [PDF-XChange Viewer](https://www.tracker-software.com/product/pdf-xchange-viewer) (Portable version). It is printing correctly and is much faster than [SumatraPDF](https://www.sumatrapdfreader.org/).

[PDF-XChange Viewer](https://www.tracker-software.com/product/pdf-xchange-viewer) allows to send PDF files to printer without graphical user interface.

[Download PDF-XChange Viewer](https://www.tracker-software.com/product/pdf-xchange-viewer/download?fileid=448)

## How to use

1. Put a copy of `PDFXCview.exe` in `C:\Windows folder` so the program will be available in any directory without specifying the path.

2. Put `print.bat` anywhere you want, for example in the desktop.

3. Set your desired printer as system default.

4. Drag the folder with PDF files to `print.bat` file to start printing all PDF files inside that folder.

5. Wait printing to finish.
